import { Component, OnInit } from '@angular/core';
import { DaysService } from '../../services/days.service';

@Component({
  selector: 'app-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.css']
})
export class DaysComponent implements OnInit {

days = null;

day={
  is:null,
  id:null,
  hours:null,
  seconds:null,
  date:null,
  cin:null,
  cout:null
}

  constructor(private daysService:DaysService) { }

  ngOnInit() {
    this.recuperarTodos();
  }

  recuperarTodos(){
    this.daysService.recuperarTodos().subscribe(result => this.days = result);
  }
  hayRegistros(){
    return true;
  }

}
