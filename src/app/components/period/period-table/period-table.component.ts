import { Component, OnInit } from '@angular/core';
import { MonthsService } from '../../../services/months.service';


@Component({
  selector: 'app-period-table',
  templateUrl: './period-table.component.html',
  styleUrls: ['./period-table.component.css']
})
export class PeriodTableComponent implements OnInit {

  month: Month = new Month();
monthResp: Month = new Month();


  constructor(private monthsService: MonthsService) { }

  ngOnInit() {
    //this.recuperarTodos();
  }

  hayRegistros(){
    return true;
  }

  recuperarTodosPeriodos(){
    this.monthsService.getMonthPeriod(this.month.imes).subscribe((result: any) => {
      this.monthResp = result;
      console.log(this.monthResp);
    });
  }
}

class Month {
  imes: string;
  isstr: string;
  id: string;
  hours: string;
  seconds: string;
}

