import { Component, OnInit } from '@angular/core';
import { MonthsService } from '../../../services/months.service';

@Component({
  selector: 'app-period-form',
  templateUrl: './period-form.component.html',
  styleUrls: ['./period-form.component.css']
})
export class PeriodFormComponent implements OnInit {

  month: Month = new Month();
monthResp: Month = new Month();


  constructor(private monthsService: MonthsService) { }

  ngOnInit() {
    //this.recuperarTodos();
  }

  hayRegistros(){
    return true;
  }

  recuperarTodosPeriodos(){
    this.monthsService.getMonthPeriod(this.month.imes).subscribe((result: any) => {
      this.monthResp = result;
      console.log(this.monthResp);
    });
  }

}

class Month {
  imes: string;
  isstr: string;
  id: string;
  hours: string;
  seconds: string;
}

