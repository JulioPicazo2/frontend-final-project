import { Component, OnInit } from '@angular/core';
import { MonthsService } from '../../../services/months.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

month: Month = new Month();
monthResp: Month = new Month();


  constructor(private monthsService: MonthsService) { }

  ngOnInit() {
    //this.recuperarTodos();
  }

  hayRegistros(){
    return true;
  }

  recuperarTodos(){
    this.monthsService.getIsAndMonth(this.month.isstr, this.month.imes).subscribe((result: any) => {
      this.monthResp = result;
      console.log(this.monthResp);
    });
  }

}


class Month {
  imes: string;
  isstr: string;
  id: string;
  hours: string;
  seconds: string;
}
