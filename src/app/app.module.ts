import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { PeriodComponent } from './components/period/period.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { UserTableComponent } from './components/users/user-table/user-table.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { PeriodFormComponent } from './components/period/period-form/period-form.component';
import { PeriodTableComponent } from './components/period/period-table/period-table.component';
import { DaysComponent } from './components/days/days.component';

import {  DaysService } from '././services/days.service';
import { LoadComponent } from './components/load/load.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PeriodComponent,
    NavbarComponent,
    HomeComponent,
    UserTableComponent,
    UserFormComponent,
    PeriodFormComponent,
    PeriodTableComponent,
    DaysComponent,
    LoadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DaysService],
  bootstrap: [AppComponent]
})
export class AppModule { }
