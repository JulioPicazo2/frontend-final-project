import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoadService {

  constructor(private http: HttpClient) { }

  cargar(){
    return this.http.get('http://localhost:8080/api/v1/update');
    console.log("Cargado");
  }
}
