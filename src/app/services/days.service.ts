import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DaysService {

  constructor(private http: HttpClient) { }

  recuperarTodos(){
    return this.http.get('http://localhost:8080/api/v1/days');
  }
}
