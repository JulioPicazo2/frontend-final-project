import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MonthsService {

  urlData: string = 'http://localhost:8080/api/v1';
  constructor(private httpClient: HttpClient) { }

   //A-USER-GET
   getIsAndMonth(is:string, m:string){
    return this.httpClient.get(`http://localhost:8080/api/v1/users/${is}/${m}`);   
  }
  getMonthPeriod(m:string){
    return this.httpClient.get(`http://localhost:8080/api/v1/period/${m}`);
    }
}
