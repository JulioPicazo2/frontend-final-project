import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './components/users/users.component';
import { PeriodComponent } from './components/period/period.component';
import { HomeComponent } from './components/home/home.component';
import { UserTableComponent } from './components/users/user-table/user-table.component';
import { PeriodTableComponent } from './components/period/period-table/period-table.component';
import { DaysComponent } from './components/days/days.component';
import { LoadComponent } from './components/load/load.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { PeriodFormComponent } from './components/period/period-form/period-form.component';


const routes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'period', component: PeriodComponent },
  { path: 'home', component: HomeComponent },
  { path: 'tableuser', component: UserTableComponent},
  { path: 'tableperiod', component: PeriodTableComponent},
  { path: 'days', component: DaysComponent},
  { path: 'load', component: LoadComponent},
  { path: 'post', component: UserFormComponent},
  { path: 'periodPost', component: PeriodFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
